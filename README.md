# ElapseTimer

ElapseTimer is a tiny library providing an abstraction of an elapsing timer.

# Description

This ElapseTimer library is implemented in plain-C and targets embedded
super-loop based applications. It supports this kind of applications to
perform tasks periodically, or after a specified time, without blocking
the super-loop.

## The ElapseTimer Layer
There is a "business layer" implemented in elapsetimer.h and elapsetimer.c.
This layer does not access any timers or clocks directly, but redirect 
these tasks to the underlying layer of "tick-sources".

## Tick-source
Tick-sources are platform dependant providers of isochronous time.

Tick-sources may directly access the hardware (or HAL variables). They
handle up- or down counting of these timers, their number range and
wrap-around.

Every ElapseTimer needs a tick-source. It inherits the time base and
resoluton. Thus the ElapseTimer itself does not have a time base, it might
be a good idea to add a corresponding postfix (e.g. ``_ms``) to its name.

This project contains implementations of tick-sources for some environments.

### Tick-source Implementation
TODO: more description needed

Two function:
* get()
* elapsed(to, from)

# Usage
## Working with ElapseTimers

There are three operations for an ElapsTimer:
* ``trigger``: get and keep the current time.
* ``get_elapsed_time``: get the time since ``trigger``
    * and optionally re-``trigger`` the timer
* ``is_elapsed``: to find out if a given time has elapsed since ``trigger``
    * and optionally re-``trigger`` the timer in various modes

### ElapseTimerMode to Re-trigger
The ``is_elapsed`` function re-triggers the ElapseTimer in different ways:
* ``DO_NOT_TRIGGER``: Never re-trigger the timer
    * let it continue to run
* ``TRIGGER``: Force-trigger the timer, regardless elapsed or not
    * can implement a watchdog by re-triggering before elapse
* ``MIN_DURATION``: "re-trigger now"
    * not considering how long the timer already has elapsed
* ``CONST_FREQUENCY``: when called in a loop, the timer will (on the long
    run) keep the frequency corresponding to the requested time.
    * actual time between ``is_elapsed`` returning true will occasionally
      be shorter then the provided time.

### Example for the Arduino Environment
```C
#include "elapsetimer.h"

#include "ticksource_arduino_millis.h"

ElapseTimer timer_ms = {.tick_source = &ticksource_arduino_millis};

void loop()
{
  if (ElapseTimer_is_elapsed(&timer_ms, 300, ELAPSE_TIMER_MIN_DURATION)) {
      /* periodically do something */
  }
}

```

# Installation
The "installation" depends on your development environment.

You can copy the files from the src folder to your project, or clone this
repository to your project's lib folder.

All what is needed is that:
* the c-file gets compiled and linked to your project
* the h-files are in the include-path.

# Project status
Long-time, low-priority project. No project plan, or actively driving this
project.

I may add features, but only if there is a current need.
## Road map
The idea of this tiny project is to keep its footprint small. So, there are
no plans to add a lot of features.

More platform-specific tick-sources might be added in the future.

# License
[MIT License](https://choosealicense.com/licenses/mit/)

# Authors and acknowledgment
See the project main repository: https://gitlab.com/swal-public/elapsetimer

Stefan Waldschmidt, inspired by lots of other projects.
