#include <Arduino.h>

#include "elapsetimer.h"

#include "ticksource_arduino_millis.h"
#include "ticksource_arduino_micros.h"
#include "ticksource_stm32hal_tick.h"

static ElapseTimer high_timer_ms = {.tick_source = &ticksource_arduino_millis};
static ElapseTimer low_timer_us = {.tick_source = &ticksource_arduino_micros};

static ElapseTimer hal_timer_ms = {.tick_source = &ticksource_stm32hal_tick};

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
}

static bool is_high = false;

void loop()
{
  if (is_high && ElapseTimer_is_elapsed(&high_timer_ms, 200, ELAPSE_TIMER_DO_NOT_TRIGGER))
  {
    digitalWrite(LED_BUILTIN, LOW);
    is_high = false;
    ElapseTimer_trigger(&low_timer_us);
  }

  if ((!is_high) && ElapseTimer_is_elapsed(&low_timer_us, 800000, ELAPSE_TIMER_DO_NOT_TRIGGER))
  {
    digitalWrite(LED_BUILTIN, HIGH);
    is_high = true;
    ElapseTimer_trigger(&high_timer_ms);
  }
}
