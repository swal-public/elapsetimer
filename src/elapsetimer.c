/**
 * @file elapsetimer.c
 * @brief Functions and data structures to implement an elapsing timer for
 *        loop-based (super loop. event loop) environments.
 * @author Stefan Waldschmidt
 * @date 2017-05-04...
 * @copyright Copyright (c) 2017 Stefan Waldschmidt.
 *            This project is released under the MIT License.
 * 
 * https://gitlab.com/swal-public/elapsetimer
 */

#include "elapsetimer.h"

void ElapseTimer_trigger(ElapseTimer *timer)
{
    timer->start_time = timer->tick_source->get();
}

unsigned long ElapseTimer_get_elapsed_time(ElapseTimer *timer,
                                           bool trigger)
{
    unsigned long now = timer->tick_source->get();
    unsigned long elapsed = timer->tick_source->elapsed(now, timer->start_time);
    if (trigger)
    {
        timer->start_time = now;
    }
    return elapsed;
}

extern bool ElapseTimer_is_elapsed(ElapseTimer *timer,
                                   unsigned long duration,
                                   ElapseTimerMode trigger_mode)
{
    unsigned long now = timer->tick_source->get();
    unsigned long elapsed = timer->tick_source->elapsed(now, timer->start_time);
    if (trigger_mode == ELAPSE_TIMER_TRIGGER)
    {
        timer->start_time = now;
    }
    if (elapsed >= duration)
    {
        if (trigger_mode == ELAPSE_TIMER_MIN_DURATION)
        {
            timer->start_time = now;
        }
        else if (trigger_mode == ELAPSE_TIMER_CONST_FREQUENCY)
        {
            timer->start_time += duration;
        }
        return true;
    }
    else
    {
        return false;
    }
}
