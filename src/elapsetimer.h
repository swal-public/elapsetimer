/**
 * @file elapsetimer.h
 * @brief Functions and data structures to implement an elapsing timer for
 *        loop-based (super loop. event loop) environments.
 * @author Stefan Waldschmidt
 * @date 2017-05-04...
 * @copyright Copyright (c) 2017 Stefan Waldschmidt.
 *            This project is released under the MIT License.
 * 
 * https://gitlab.com/swal-public/elapsetimer
 */

#ifndef ELAPSE_TIMER_H
#define ELAPSE_TIMER_H

#include "elapsetimer_ticksource.h"

#if defined(__cplusplus)
extern "C"
{
#endif

#include <stdbool.h>

    /**
     * @brief Type (class) to an ElapseTimer.
     *
     * The application needs to allocate the memory (which is all memory
     * the instance needs) for every instance of an ElapseTimer and ensure
     * the persistency of this memory during its life time.
     */
    typedef struct ElapseTimer
    {
        const ElapseTimerTickSource * const tick_source;
        unsigned long start_time;
    } ElapseTimer;

    /**
     * @brief Set the timer to the current time.
     *
     * @param[in,out] timer  Instance of a timer.
     */
    extern void ElapseTimer_trigger(ElapseTimer *timer);

    /**
     * @brief Get the elapsed time since last trigger.
     *
     * The timer can be triggered (restarted), if needed.
     *
     * @param[in,out] timer  Instance of a timer.
     * @param[in] trigger  <code>true</code> re-triggers the timer at he same
     *                     time.
     * @return  The elapsed time since last trigger.
     */
    extern unsigned long ElapseTimer_get_elapsed_time(ElapseTimer *timer,
                                                      bool trigger);

    /**
     * @brief Modes to re-trigger an ElapseTimer.
     */
    typedef enum ElapseTimerMode
    {
        ELAPSE_TIMER_DO_NOT_TRIGGER, /**< Do not trigger, let it run. */
        ELAPSE_TIMER_TRIGGER,        /**< Always force trigger (watchdog). */
        ELAPSE_TIMER_MIN_DURATION,   /**< Set to current time. */
        ELAPSE_TIMER_CONST_FREQUENCY /**< Set to time it actually elapsed. */
    } ElapseTimerMode;

    /**
     * @brief Check it the timer elapsed, optionally re-trigger it.
     *
     * Check if the timer was started at least <code>time</code> ago.
     * Optionally re-trigger the timer in different ways (see ElapseTimerMode).
     *
     * @param[in,out] timer  Instance of a timer.
     * @param[in] time  Period to check if timer was triggered before.
     * @param[in] mode  The timer can be re-triggered (see ElapseTimerMode).
     */
    extern bool ElapseTimer_is_elapsed(ElapseTimer *timer,
                                       unsigned long time,
                                       ElapseTimerMode mode);

#if defined(__cplusplus)
}
#endif

#endif
