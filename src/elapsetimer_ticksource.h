/**
 * @file elapsetimer_ticksource.h
 * @brief Abstract interface for every tick sources.
 * @author Stefan Waldschmidt
 * @date 2017-05-04...
 * @copyright Copyright (c) 2017 Stefan Waldschmidt.
 *            This project is released under the MIT License.
 * 
 * https://gitlab.com/swal-public/elapsetimer
 */

#ifndef ELAPSE_TIMER_TICK_SOURCE_H
#define ELAPSE_TIMER_TICK_SOURCE_H

/**
 * @brief Interface of a protform-specific Tick Source.
 * 
 * Every ElapseTimer needs a Tick Source.
 */
typedef struct ElapseTimerTickSource
{
    /**
     * @brief Get the current time of the Tick Source.
     * 
     * The actual value needs to be an unsigned long. But values, their
     * value range or interpretation is of no meaning to the caller.
     * The value will only be used to be passed in to the ``elapsed``
     * function, either as ``later`` or ``earlier``-
     * 
     * @return Current time which can (only) be interpreted by ``elapsed``.
     */
    unsigned long (*get)(void);

    /**
     * @brief Get the elapsed time from ''earlier'' to ''later''.
     * 
     * The time base of the upper-layer ElapseTimer is actually determined
     * by this function.
     * 
     * @param later  A time returned by ``get``. Later than ``earlier``.
     *               Typically "now".
     * @param earlier A time returned by an earlier call of ``get``.
     * @return The time from ``earlier`` to ``later``. The documentation of
     *         the Tick Source should clearely specify the time unit it its
     *         documentation (or name).
     */
    unsigned long (*elapsed)(unsigned long later, unsigned long earlier);
} ElapseTimerTickSource;

#endif
