/**
 * @file elapsetimer_ticksource.h
 * @brief Tick Source based on the ``millis`` function of the Arduino library.
 * @author Stefan Waldschmidt
 * @date 2023-12-02
 * @copyright Copyright (c) 2023 Stefan Waldschmidt.
 *            This project is released under the MIT License.
 * 
 * https://gitlab.com/swal-public/elapsetimer
 */

#ifndef TICK_SOURCE_ARDUINO_MILLLIS_H
#define TICK_SOURCE_ARDUINO_MILLLIS_H

#include "elapsetimer_ticksource.h"

#include <Arduino.h>

static unsigned long arduinomillis_get(void)
{
    return millis();
}

static unsigned long arduinomillis_elapsed(unsigned long later,
                                           unsigned long earlier)
{
    /* "millis" are increasing and of type uint32_t */
    return ((uint32_t)later) - ((uint32_t)earlier);
}

static const ElapseTimerTickSource ticksource_arduino_millis = {
    arduinomillis_get, arduinomillis_elapsed};

#endif
