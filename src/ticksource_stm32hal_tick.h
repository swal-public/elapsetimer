/**
 * @file ticksource_stm32hal_tick.h
 * @brief Tick Source based on ``HAL_GetTick`` function of the STM32 HAL.
 * @author Stefan Waldschmidt
 * @date 2023-12-02
 * @copyright Copyright (c) 2023 Stefan Waldschmidt.
 *            This project is released under the MIT License.
 * 
 * https://gitlab.com/swal-public/elapsetimer
 * 
 * Warning: STM32-HAL ticks typically are one millisecond. But they might
 *          be configured differently in your application.
 */

#ifndef TICK_SOURCE_STM32HAL_TICK_H
#define TICK_SOURCE_STM32HAL_TICK_H

#include "elapsetimer_ticksource.h"

/* TODO: This include is valid for STM32F7 micro-controllers.
 * The HAL of other STM32 derivats use different include file names.
 */
// #include "stm32f7xx_hal.h"

#include "stm32c0xx_hal.h"

static unsigned long stm32hal_tick_get(void)
{
    return HAL_GetTick();
}

static unsigned long stm32hal_tick_elapsed(unsigned long later,
                                           unsigned long earlier)
{
    /* "ticks" are increasing and of type uint32_t */
    return ((uint32_t)later) - ((uint32_t)earlier);
}

static const ElapseTimerTickSource ticksource_stm32hal_tick = {
    stm32hal_tick_get, stm32hal_tick_elapsed};

#endif
